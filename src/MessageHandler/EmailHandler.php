<?php

namespace App\MessageHandler;

use App\Message\Email as MessageEmail;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class EmailHandler implements MessageHandlerInterface
{
    public function __construct(
        private MailerInterface $mailerInterface
    ) {
    }

    public function __invoke(MessageEmail $message)
    {
        $mail = new Email();
        $mail->from($message->getSender() . "@my-service-mailer.com");
        $mail->to($message->getReceiver());
        $mail->subject($message->getSubject());
        $mail->text($message->getText());

        $this->mailerInterface->send($mail);
    }
}
