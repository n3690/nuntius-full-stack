<?php

namespace App\Controller;

use App\Form\UserNameFormType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    #[Route('/', name: 'app_home')]
    public function home(
        Session $session,
        MailerInterface $mailerInterface
    ): Response {

        if ($session->has('USER_NAME')) {
            return $this->render('home/index.html.twig', []);
        } else {
            return $this->redirectToRoute('app_login');
        }
    }

    #[Route('/login', name: 'app_login')]
    public function login(
        Session $session,
        Request $request
    ): Response {
        if ($session->has('USER_NAME')) {
        }

        $usernameForm = $this->createForm(UserNameFormType::class)->handleRequest($request);

        if ($usernameForm->isSubmitted() && $usernameForm->isValid()) {
            $session->set('USER_NAME', $usernameForm->getData()['username']);
            return $this->redirectToRoute('app_home');
        }

        return $this->render('login/index.html.twig', [
            'user_name' => $usernameForm->createView()
        ]);
    }

    #[Route('/logout', name: 'app_logout')]
    public function logout(
        Session $session,
    ): Response {
        $session->clear();

        return $this->redirectToRoute('app_home');
    }
}
