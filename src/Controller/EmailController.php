<?php

namespace App\Controller;

use App\Form\EmailFormType;
use App\Message\Email as MessageEmail;
use Symfony\Component\Mime\Email;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Mime\Message;

class EmailController extends AbstractController
{
    #[Route('/email', name: 'app_email')]
    public function index(
        MailerInterface $mailerInterface,
        MessageBusInterface $messageBusInterface,
        Session $session,
        Request $request
    ): Response {

        $message = new MessageEmail();
        $message->setSender($session->get('USER_NAME'));

        $form = $this->createForm(EmailFormType::class, $message)->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $messageBusInterface->dispatch($message);
            $this->addFlash('success', 'Votre email a bien été envoyé !');

            return $this->redirectToRoute('app_email');
        }

        return $this->render('email/index.html.twig', [
            'controller_name' => 'EmailController',
            'form' => $form->createView()
        ]);
    }
}
