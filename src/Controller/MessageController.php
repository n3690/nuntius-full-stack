<?php

namespace App\Controller;

use Symfony\Component\Mercure\Update;
use Symfony\Component\Mercure\HubInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Session\Session;

class MessageController extends AbstractController
{
    #[Route('/message', name: 'app_message')]
    public function message(): Response
    {
        return $this->render('message/index.html.twig', [
            'controller_name' => 'MessageController',
        ]);
    }

    #[Route('/message/send', name: 'api_message')]
    public function sendMessage(
        Request $request,
        HubInterface $hubInterface,
        Session $session
    ): Response {
        if ($request->toArray()['message']) {
            $update = new Update(
                'http://127.0.0.1:801/messages/all',
                json_encode([
                    'sender' => $session->get('USER_NAME'),
                    'message' => $request->toArray()['message']
                ])
            );

            $hubInterface->publish($update);
            return $this->json([
                'status' => 'Ok',
            ]);
        } else {
            return $this->json([
                'status' => 'Not send',
            ], 400);
        }
    }

    #[Route('/message/generate', name: 'api_message_generate')]
    public function generate(
        Session $session,
        HubInterface $hubInterface,
    ): Response {
        $userList = array(
            "Nathalie Arthaud",
            "Nicolas Dupont-Aignan",
            "Anne Hidalgo",
            "Yannick Jadot",
            "Jean Lassalle",
            "Marine Le Pen",
            "Emmanuel Macron",
            "Jean-Luc Mélenchon",
            "Valérie Pécresse",
            "Philippe Poutou",
            "Fabien Roussel",
            "Éric Zemmour",
            "Emilien Gantois",
            $session->get('USER_NAME')
        );

        $sentencesList = array(
            'Emmanuel Macron aime le débat.',
            'Ça roule mieux, ça roule vraiment mieux à Paris.',
            'Si je suis élue, je dirai à Vladimir Poutine que nous devons construire la paix en Europe.',
            'Les stations-service sont le seul endroit en France où celui qui tient le pistolet est aussi celui qui se fait braquer.',
            'Je serai la présidente du régalien, de la renaissance démocratique, du quotidien, de la concorde restaurée entre tous les Français, de la justice, de la fraternité nationale, de la paix civile.',
            "Les femmes n'expriment pas le pouvoir, elles ne l'incarnent pas, c'est comme ça. Le pouvoir s'évapore dès qu'elles arrivent.",
            "Regardez, dans les milieux où il y a vraiment le pouvoir, il n'y a pas de femmes. Dans la finance, c'est infinitésimale, c'est marginal.",
            "Je vais ressortir le Karcher de la cave. Cela fait dix ans qu’il y est et il est temps de l’utiliser. Il s’agit de remettre de l’ordre dans la rue.",
            "Je suis une femme « libérée-délivrée ».",
            "Ma valeur c'est le travail, pas l'assistanat.",
            "Nous quand on est convoqué par la police, on n'a pas d'immunité ouvrière.",
            "Hollande est satisfait de son bilan ; c'est pour cela qu'il le dépose.",
            "Il ne faut pas donner de voix à Mme Le Pen ! Il ne faut pas donner une seule voix à Mme Le Pen !",
            "Rassemblez-vous avec moi, tous ceux qui sont fâchés mais pas facho, venez vous mettre en colère avec moi.",
            "La République, c'est moi !",
            "Dans une démocratie, la personne d'un parlementaire est intouchable",
            "Ne me touchez pas ! Vous n'avez pas le droit de me toucher ! Personne ne me touche, ma personne est sacrée. Je suis parlementaire. Pour vous en convaincre...",
            "France belle et rebelle, vienne le temps des cerises et des jours heureux.",
            "La boulimie c'est dans la tête !",
            "ça vous tente un plan à 4 ?",
            "Si elle devient grosse j'la quitte.",
            "Elle la j'la soulève.",
            "La meuf même avec un bâton je la touche pas"

        );

        for ($i = 0; $i < 10; $i++) {
            $update = new Update(
                'http://127.0.0.1:801/messages/all',
                json_encode([
                    'sender' => $userList[rand(0, count($userList) - 1)],
                    'message' => $sentencesList[rand(0, count($sentencesList) - 1)]
                ])
            );

            $hubInterface->publish($update);
        }


        return $this->json([
            'status' => 'Ok',
            'users' => count($userList),
            'sentences' => count($sentencesList),
            "selected" => [
                'user' => rand(0, count($userList) - 1),
                'sentences' => rand(0, count($sentencesList) - 1),
            ]
        ]);
    }
}
